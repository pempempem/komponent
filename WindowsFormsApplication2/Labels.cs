﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    public class Labels
    {
        private List<int> labelx;
        private List<int> labely;
        private int inputsX;
        private int inputsY;

        public int InputsX
        {
            get{ return inputsX; }
            set { inputsX = value; }
        }
        public int InputsY
        {
            get { return inputsY; }
            set { inputsY = value; }
        }
        public List<int> Labelx
        {
            get { return labelx; }
        }

        public List<int> Labely
        {
            get { return labely; }
        }
        public Labels()
        {
            labelx = new List<int>();
            labely = new List<int>();
        }

        public Labels(int inputsX,int inputsY)
        {
            labelx = new List<int>();
            labely = new List<int>();
            this.inputsX = inputsX;
            this.inputsY = inputsY;
        }

        public void expandLabels(int[] expanderX,int[] expanderY)
        {
            labelx.AddRange(expanderX);
            labely.AddRange(expanderY);
        }

        public void expandInputs(int x,int y)
        {
            inputsX =x;
            inputsY =y;
        }
    }
}
