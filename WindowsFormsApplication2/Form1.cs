﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{

    public partial class Form1 : Form
    {
        private int[] labelx;
        private int[] labely;

        String[,] data = { {"a","b","c","d"}, {"e","f","g","h"},
                        { "a", "b", "c", "d" }, { "e", "f", "g", "h" }, 
                        { "a", "b", "c", "d" }, { "e", "f", "g", "h" },
                            {"a","b","c","d"}, {"e","f","g","h"}};

        public Form1()
        { 
            InitializeComponent();
        }

        private String[,] dataGenerator(Labels lab)
        {
            Random rand = new Random();
            int x = 1, y = 1;

            foreach(int i in lab.Labelx)
                x *= i;
            foreach (int i in lab.Labely)
                y *= i;
            String[,] answer = new String[x, y];
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    answer[i,j] = ((char)rand.Next(65, 74)).ToString();
            return answer;
        } 

        private void button1_Click(object sender, EventArgs e)
        {
            komponent1.enlargePanel();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            String labelx = textBox2.Text;
            this.labelx = new int[labelx.Length];
            List<String[]> labelXvalues = new List<string[]>();
            int multiplierX = 1;

            for (int i = 0; i < labelx.Length; i++)
            {
                this.labelx[i] = Convert.ToInt32(labelx[i].ToString());
                String[] tab = new String[this.labelx[i]* multiplierX];
                for (int j = 0; j < this.labelx[i] * multiplierX; j++)
                    tab[j] = ((char)rand.Next(65, 74)).ToString();
                multiplierX *= this.labelx[i];
                labelXvalues.Add(tab);
            }

            String labely = textBox3.Text;
            this.labely = new int[labely.Length];
            List<String[]> labelYvalues = new List<string[]>();
            int multiplierY = 1;

            for (int i = 0; i < labely.Length; i++)
            {
                this.labely[i] = Convert.ToInt32(labely[i].ToString());
                String[] tab = new String[this.labely[i]* multiplierY];
                for (int j = 0; j < this.labely[i] * multiplierY; j++)
                    tab[j] = ((char)rand.Next(65, 74)).ToString();
                multiplierY *= this.labely[i];
                labelYvalues.Add(tab);
            }

            



            Labels lab = new Labels();
            if (labelx != null && labely != null)
                lab.expandLabels(this.labelx, this.labely);
            String[,] data = dataGenerator(lab);
            lab.expandInputs(this.labelx.Length*2, this.labely.Length*2);
            

            komponent1.setData(data, lab, labelXvalues, labelYvalues);
        }


    }
}
