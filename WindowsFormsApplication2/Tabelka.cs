﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Tabelka : UserControl
    {

        private Point startPoint = new Point(50,50);
        private int cellH = 30;
        private int cellW = 30;
        private int fontSize = 16;
        private int margin = (30-16)/3;
        private String[,] data;

        private bool isDataCaptured = false;

        private horizontalLabel hL = new horizontalLabel();
        private verticalLabel vL = new verticalLabel();

        int[] labelH;
        int[] labelV;

        List<String[]> labelMarkX = new List<string[]>();
        List<String[]> labelMarkY = new List<string[]>();

        //LABELS NAMES
        int[] labels_Names_X;
        int[] labels_Names_Y;

        

        Brush[,] palletes = 
            { 
            {Brushes.AliceBlue,Brushes.AntiqueWhite,Brushes.Aqua,Brushes.Aquamarine,Brushes.Azure,Brushes.Beige,Brushes.Bisque, Brushes.BlanchedAlmond,Brushes.Blue,Brushes.BlueViolet,Brushes.Brown,Brushes.BurlyWood}, 
            {Brushes.CadetBlue,Brushes.Chartreuse,Brushes.Chocolate,Brushes.Coral,Brushes.CornflowerBlue,Brushes.Cornsilk,Brushes.Crimson,Brushes.Cyan,Brushes.DarkBlue,Brushes.DarkCyan,Brushes.DarkGoldenrod,Brushes.DarkGray}
             };
        private struct horizontalLabel
        {
            public int width;
            public int height;
            public int laneHeight;
            public int horizontalNum;
            public int numberOfColumns;

            public void defineHL(int width,int height,int laneHeight,int horizontalNum,int numberOfColumns)
            {
                this.width = width;
                this.height = height;
                this.laneHeight = laneHeight;
                this.horizontalNum = horizontalNum;
                this.numberOfColumns = numberOfColumns;
            }
        }

        private struct verticalLabel
        {
            public int width;
            public int height;
            public int laneWidth;
            public int verticalNum;
            public int numberOfRows;

            public void defineVL(int width, int height, int laneWidth,int verticalNum,int numberOfRows)
            {
                this.width = width;
                this.height = height;
                this.laneWidth = laneWidth;
                this.verticalNum = verticalNum;
                this.numberOfRows = numberOfRows;
            }
        }


        private Brush[,] getColors(String[,] data,int palleteNum)
        {
            Brush[,] set = new Brush[data.GetLength(0),data.GetLength(1)];
            Dictionary<String, Brush> sorted = new Dictionary<String, Brush>();
            List<String> usedSymbols = sorted.Keys.ToList<String>();

            bool repeatOrNot = false;
            int pallete_counter = 0;
            for (int i = 0; i < data.GetLength(0); i++)
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    foreach (String u in usedSymbols)
                    {
                        if (data[i, j].Equals(u))
                        {
                            repeatOrNot = true;
                            break;
                        }
                    }
                    if (repeatOrNot)
                    {
                        set[i, j] = sorted[data[i, j]];
                        repeatOrNot = false;
                    }
                    else
                    {
                        sorted.Add(data[i, j], palletes[palleteNum, pallete_counter++]);
                        usedSymbols = sorted.Keys.ToList<String>();
                        set[i, j] = sorted[data[i,j]];
                    }
                }
            return set;
        }

        public Tabelka()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //-------
            base.OnPaint(e);
            if (isDataCaptured)
            {
                Point leftUpperCorner = new Point(startPoint.X + vL.width, startPoint.Y + hL.height);
                Point rightUpperCorner = new Point(leftUpperCorner.X + hL.width, leftUpperCorner.Y);
                Point leftLowerCorner = new Point(leftUpperCorner.X, leftUpperCorner.Y + vL.height);
                Point rightLowerCorner = new Point(rightUpperCorner.X, rightUpperCorner.Y + vL.height);

                Brush[,] colors = getColors(data, 1);

                //data
                for (int i = 0; i < data.GetLength(0); i++)
                    for (int j = 0; j < data.GetLength(1); j++)
                    {
                        Point start = new Point(leftUpperCorner.X + cellH * i + margin, leftUpperCorner.Y + cellH * j + margin);
                        Rectangle cell = new Rectangle(leftUpperCorner.X + cellH*i, leftUpperCorner.Y + cellH * j, cellW, cellH);
                        e.Graphics.FillRectangle(colors[i,j], cell);
                        e.Graphics.DrawString(data[i, j], new Font("Arial", fontSize), Brushes.Black, start);

                    }

                //grid
                e.Graphics.DrawLine(Pens.Black, leftUpperCorner,rightUpperCorner);
                e.Graphics.DrawLine(Pens.Black, leftUpperCorner, leftLowerCorner);
                e.Graphics.DrawLine(Pens.Black, rightUpperCorner, rightLowerCorner);
                e.Graphics.DrawLine(Pens.Black, leftLowerCorner, rightLowerCorner);

                for (int i = 0; i <= hL.numberOfColumns; i++)
                {
                    Point start = new Point(leftUpperCorner.X + cellH*i,leftUpperCorner.Y );
                    Point stop = new Point(leftLowerCorner.X + cellH*i, leftLowerCorner.Y );
                    e.Graphics.DrawLine(Pens.Black, start, stop);
                }
                for (int i = 0; i <= vL.numberOfRows; i++)
                {
                    Point start = new Point(leftUpperCorner.X , leftUpperCorner.Y + cellW*i);
                    Point stop = new Point(rightUpperCorner.X, rightUpperCorner.Y + cellW*i);
                    e.Graphics.DrawLine(Pens.Black, start, stop);
                }

                this.Width = startPoint.X * 2 + rightUpperCorner.X;
                this.Height = startPoint.Y * 2 + leftLowerCorner.Y;

                //labels
                Point leftCornerHL = new Point(startPoint.X + vL.width, startPoint.Y);
                Point rightCornerHL = new Point(leftUpperCorner.X + hL.width, startPoint.Y);
                e.Graphics.DrawLine(Pens.Black, leftCornerHL, rightCornerHL);
                e.Graphics.DrawLine(Pens.Black, leftCornerHL, leftUpperCorner);
                e.Graphics.DrawLine(Pens.Black, rightCornerHL, rightUpperCorner);

                Point leftCornerVL = new Point(startPoint.X, startPoint.Y + hL.height );
                Point rightCornerVL = new Point(startPoint.X, leftLowerCorner.Y);
                e.Graphics.DrawLine(Pens.Black, leftCornerVL, rightCornerVL);
                e.Graphics.DrawLine(Pens.Black, leftCornerVL, leftUpperCorner);
                e.Graphics.DrawLine(Pens.Black, rightCornerVL, leftLowerCorner);

                int jH = 0;
                int[] labelHloc = new int[labelH.Length];
                for (int i = 0; i < labelHloc.Length; i++)
                    labelHloc[i] = labelH[i];
                for(int i=0;i<hL.horizontalNum;i++)
                {
                    Point start = new Point(leftCornerHL.X , leftCornerHL.Y + cellH * i);
                    Point stop = new Point(rightCornerHL.X, rightCornerHL.Y + cellH * i);
                    e.Graphics.DrawLine(Pens.Black, start, stop);

                    if (i % 2 == 1)
                    {
                        jH++;
                        labelHloc[jH] = labelHloc[jH] * labelHloc[jH - 1];
                    }
                    try
                    {
                        if (jH < labelHloc.Length )
                        { 
                            for (int j = 1; j <= labelHloc[jH] ; j++)
                            {
                                Point divStart = new Point(leftCornerHL.X + (hL.width / labelHloc[jH]) * j, leftCornerHL.Y + cellH * i);
                                Point divStop = new Point(leftCornerHL.X + (hL.width / labelHloc[jH])*j, rightCornerHL.Y + cellH * (i + 1));
                                e.Graphics.DrawLine(Pens.Black, divStart, divStop);

                                Point label = new Point(leftCornerHL.X + (hL.width / labelHloc[jH]) * (j-1) + (hL.width / labelHloc[jH])/2 - 10, leftCornerHL.Y + cellH * i);

                                // addition 

                                if (i % 2 == 1)
                                    e.Graphics.DrawString(labelMarkX.ElementAt((i - 1) / 2)[j - 1], new Font("Arial", fontSize), Brushes.Black, label);
                                else
                                {
                                    e.Graphics.DrawString("x", new Font("Arial", fontSize), Brushes.Black, label);
                                    label = new Point(leftCornerHL.X + (hL.width / labelHloc[jH]) * (j - 1) + (hL.width / labelHloc[jH]) / 2 - 10 + 14, leftCornerHL.Y + cellH * i + 16);
                                    e.Graphics.DrawString(labels_Names_X[i / 2].ToString(), new Font("Arial", 10), Brushes.Black, label);
                                }
                            }
                        }
                    }
                    catch(IndexOutOfRangeException)
                    {

                    }
                }


                int jV = 0;
                int[] labelVloc = new int[labelV.Length];
                for (int i = 0; i < labelVloc.Length; i++)
                    labelVloc[i] = labelV[i];
                for (int i = 0; i < vL.verticalNum; i++)
                {
                    Point start = new Point(leftCornerVL.X + cellW * i, leftCornerVL.Y);
                    Point stop = new Point(rightCornerVL.X + cellW * i, rightCornerVL.Y);
                    e.Graphics.DrawLine(Pens.Black, start, stop);

                    if (i % 2 == 1)
                    {
                        jV++;
                        labelVloc[jV] = labelVloc[jV] * labelVloc[jV - 1];
                    }
                    try
                    {

                        if (jV < labelVloc.Length)
                        {

                            for (int j = 1; j <= labelVloc[jV]; j++)
                            {
                                Point divStart = new Point(leftCornerVL.X + cellW * i, leftCornerVL.Y + (vL.height / labelVloc[jV]) * j);
                                Point divStop = new Point(leftCornerVL.X + cellW * (i+1), leftCornerVL.Y + (vL.height / labelVloc[jV]) * j);
                                e.Graphics.DrawLine(Pens.Black, divStart, divStop);

                                Point label = new Point(leftCornerVL.X + cellW * i, leftCornerVL.Y + (vL.height / labelVloc[jV]) * (j-1) + (vL.height / labelVloc[jV])/2 - 10);

                                // addition

                                if (i % 2 == 1)
                                    e.Graphics.DrawString(labelMarkY.ElementAt((i - 1) / 2)[j - 1], new Font("Arial", fontSize), Brushes.Black, label);
                                else
                                {
                                    e.Graphics.DrawString("x" , new Font("Arial", fontSize), Brushes.Black, label);
                                    label = new Point(leftCornerVL.X + cellW * i + 14, leftCornerVL.Y + (vL.height / labelVloc[jV]) * (j - 1) + (vL.height / labelVloc[jV]) / 2 - 10 + 16);
                                    e.Graphics.DrawString( labels_Names_Y[i / 2].ToString(), new Font("Arial", 10), Brushes.Black, label);
                                }
                            }

                        }
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                }

                

            }
                
        }
        public void captureData(String[,] values,Labels labels, List<String[]> labelMarkX, List<String[]> labelMarkY)
        {
            int horizontalNum = labels.InputsX;
            int verticalNum = labels.InputsY;
            // correct the math
            hL.defineHL(30*values.GetLength(0),30*horizontalNum,30,horizontalNum,values.GetLength(0));
            vL.defineVL(30 * verticalNum, 30 * values.GetLength(1), 30,verticalNum, values.GetLength(1));

            this.labelMarkX = labelMarkX;
            this.labelMarkY = labelMarkY;

            data = values;
            labels.Labelx.Insert(0, 1);
            labelH = labels.Labelx.ToArray();
            labels.Labely.Insert(0, 1);
            labelV = labels.Labely.ToArray();

            isDataCaptured = true;
            nameLabels(hL.horizontalNum, vL.verticalNum, out labels_Names_X, out labels_Names_Y);
            Refresh();
        }

        private int nameLabels(int label_X_length, int label_Y_length,out int[] label_X,out int[] label_Y) // returns table of ints
        {
            int x = label_X_length/2, y = label_Y_length/2;
            int[] labelX = new int[x],labelY = new int[y];
            int flag = 0;

            if ((x - y) == -1 && x % 2 == 0)
            {
                for (int i = 0; i < x; i++)
                {
                    if (i % 2 == 0)
                    {
                        labelX[i] = 2 + i * 2;
                        labelY[i] = 3 + i * 2;
                    }
                    if (i % 2 == 1)
                    {
                        labelY[i] = 4 + (i - 1) * 2;
                        labelX[i] = 1 + (i - 1) * 2;
                    }
                }
                labelY[x] = 1 + x * 2;

                flag = 1;
            }
            else if (x % 2 == 1 && y % 2 == 1 && x == y)
            {
                for (int i = 0; i < (x - 1); i++)
                {
                    if (i % 2 == 0)
                    {
                        labelX[i] = 2 + i * 2;
                        labelY[i] = 3 + i * 2;
                    }
                    if (i % 2 == 1)
                    {
                        labelY[i] = 4 + (i - 1) * 2;
                        labelX[i] = 1 + (i - 1) * 2;
                    }
                }
                labelX[x - 1] = 2 + (x - 1) * 2;
                labelY[x - 1] = 1 + (x - 1) * 2;

                flag = 1;
            }
            else if (x % 2 == 0 && y % 2 == 0 && x == y)
            {
                for (int i = 0; i < x; i++)
                {
                    if (i % 2 == 0)
                    {
                        labelX[i] = 2 + i * 2;
                        labelY[i] = 3 + i * 2;
                    }
                    if (i % 2 == 1)
                    {
                        labelY[i] = 4 + (i - 1) * 2;
                        labelX[i] = 1 + (i - 1) * 2;
                    }
                }
                flag = 1;
            }
            else
                MessageBox.Show("Niepoprawna konfiguracja wejść!");
            Array.Reverse(labelX);
            label_X = labelX;
            Array.Reverse(labelY);
            label_Y = labelY;
            return flag;
        }
    }
}
