﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Komponent : UserControl
    {
        public Komponent()
        {
            InitializeComponent();
        }

        public void enlargePanel()
        {
            tabelka1.Width += 20;
            tabelka1.Height += 20;
        }

        public void setData(String[,] data,Labels inputs,List<String[]> labelMarkX, List<String[]> labelMarkY)
        {
            tabelka1.captureData(data,inputs,labelMarkX,labelMarkY);
        }
    }
}
